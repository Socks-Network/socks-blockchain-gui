# Introduction

Welcome to the socks-blockchain GUI project!

-------------------

This is a fork of [socks-blockchain-gui](https://bitbucket.org/Socks-Network/socks-blockchain-gui) for [Socks blockchain](https://bitbucket.org/Socks-Network/socks-blockchain)


### Copyright

By contributing to this repository, you agree to license your work under the Apache License Version 2.0, or the MIT License, or release your work to the public domain. Any work contributed where you are not the original author must contain its license header with the original author(s) and be in the public domain, or licensed under the Apache License Version 2.0 or the MIT License.
