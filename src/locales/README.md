# Localization

Thanks for helping to translate the GUI for Socks Blockchain.

Please head over to our [Crowdin project](https://crowdin.com/project/socks-blockchain/) and add/edit translations there.
