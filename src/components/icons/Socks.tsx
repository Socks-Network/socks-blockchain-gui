import React from 'react';
import { SvgIcon, SvgIconProps } from '@material-ui/core';
import { ReactComponent as SocksIcon } from './images/socks_circle.svg';

export default function Keys(props: SvgIconProps) {
  return <SvgIcon component={SocksIcon} viewBox="-20 0 100 58" {...props} />;
}
