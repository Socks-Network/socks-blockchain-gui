enum Unit {
  SOCK = 'SOCK',
  MSOCK = 'MSOCK',
  COLOURED_COIN = 'COLOUREDCOIN',
}

export default Unit;
