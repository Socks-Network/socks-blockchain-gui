const socks = require('../../util/socks');

describe('socks', () => {
  it('converts number msock to socks', () => {
    const result = socks.msock_to_socks(1000000);

    expect(result).toBe(0.000001);
  });
  it('converts string msock to socks', () => {
    const result = socks.msock_to_socks('1000000');

    expect(result).toBe(0.000001);
  });
  it('converts number msock to socks string', () => {
    const result = socks.msock_to_socks_string(1000000);

    expect(result).toBe('0.000001');
  });
  it('converts string msock to socks string', () => {
    const result = socks.msock_to_socks_string('1000000');

    expect(result).toBe('0.000001');
  });
  it('converts number socks to msock', () => {
    const result = socks.socks_to_msock(0.000001);

    expect(result).toBe(1000000);
  });
  it('converts string socks to msock', () => {
    const result = socks.socks_to_msock('0.000001');

    expect(result).toBe(1000000);
  });
  it('converts number msock to colouredcoin', () => {
    const result = socks.msock_to_colouredcoin(1000000);

    expect(result).toBe(1000);
  });
  it('converts string msock to colouredcoin', () => {
    const result = socks.msock_to_colouredcoin('1000000');

    expect(result).toBe(1000);
  });
  it('converts number msock to colouredcoin string', () => {
    const result = socks.msock_to_colouredcoin_string(1000000);

    expect(result).toBe('1,000');
  });
  it('converts string msock to colouredcoin string', () => {
    const result = socks.msock_to_colouredcoin_string('1000000');

    expect(result).toBe('1,000');
  });
  it('converts number colouredcoin to msock', () => {
    const result = socks.colouredcoin_to_msock(1000);

    expect(result).toBe(1000000);
  });
  it('converts string colouredcoin to msock', () => {
    const result = socks.colouredcoin_to_msock('1000');

    expect(result).toBe(1000000);
  });
});
