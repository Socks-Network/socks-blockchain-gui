export default {
  palette: {
    primary: {
      main: '#c16d00',
      contrastText: '#ffffff',
    },
    secondary: {
      main: '#000000',
      contrastText: '#ffffff',
    },
    danger: {
      main: '#dc3545',
      contrastText: '#ffffff',
    },
  },
  drawer: {
    width: '100px',
  },
};
