const Big = require('big.js');
const units = require('./units');

// TODO: use bigint instead of float
const convert = (amount, from, to) => {
  if (Number.isNaN(Number.parseFloat(amount)) || !Number.isFinite(amount)) {
    return 0;
  }

  const amountInFromUnit = Big(amount).times(units.getUnit(from));

  return Number.parseFloat(amountInFromUnit.div(units.getUnit(to)));
};

class Socks {
  constructor(value, unit) {
    this._value = value;
    this._unit = unit;
  }

  to(newUnit) {
    this._value = convert(this._value, this._unit, newUnit);
    this._unit = newUnit;

    return this;
  }

  value() {
    return this._value;
  }

  format() {
    const displayUnit = units.getDisplay(this._unit);

    const { format, fractionDigits, trailing } = displayUnit;

    let options = { maximumFractionDigits: fractionDigits };

    if (trailing) {
      options = { minimumFractionDigits: fractionDigits };
    }

    let value;

    if (fractionDigits !== undefined) {
      const fractionPower = Big(10).pow(fractionDigits);
      value = Number.parseFloat(
        Big(Math.floor(Big(this._value).times(fractionPower))).div(
          fractionPower,
        ),
      );
    } else {
      value = this._value;
    }

    let formatted = format.replace(
      '{amount}',
      Number.parseFloat(value).toLocaleString(undefined, options),
    );

    if (displayUnit.pluralize && this._value !== 1) {
      formatted += 's';
    }

    return formatted;
  }

  toString() {
    const displayUnit = units.getDisplay(this._unit);
    const { fractionDigits } = displayUnit;
    const options = { maximumFractionDigits: fractionDigits };
    return Number.parseFloat(this._value).toLocaleString(undefined, options);
  }
}

export const socks_formatter = (value, unit) => new Socks(value, unit);

socks_formatter.convert = convert;
socks_formatter.setDisplay = units.setDisplay;
socks_formatter.setUnit = units.setUnit;
socks_formatter.getUnit = units.getUnit;
socks_formatter.setFiat = (currency, rate, display = null) => {
  units.setUnit(currency, 1 / rate, display);
};

export const msock_to_socks = (msock) => {
  return socks_formatter(Number.parseInt(msock), 'msock').to('socks').value();
};

export const socks_to_msock = (socks) => {
  return socks_formatter(Number.parseFloat(Number(socks)), 'socks')
    .to('msock')
    .value();
};

export const msock_to_socks_string = (msock) => {
  return socks_formatter(Number(msock), 'msock').to('socks').toString();
};

export const msock_to_colouredcoin = (msock) => {
  return socks_formatter(Number.parseInt(msock), 'msock')
    .to('colouredcoin')
    .value();
};

export const colouredcoin_to_msock = (colouredcoin) => {
  return socks_formatter(Number.parseFloat(Number(colouredcoin)), 'colouredcoin')
    .to('msock')
    .value();
};

export const msock_to_colouredcoin_string = (msock) => {
  return socks_formatter(Number(msock), 'msock').to('colouredcoin').toString();
};
